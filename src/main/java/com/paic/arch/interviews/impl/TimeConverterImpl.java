package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.enums.SecondEnum;

/***
 * 
 * TimeConverter接口的一个实现类
 * 
 * @author Liu
 *
 */
public class TimeConverterImpl implements TimeConverter {
	//秒钟显示(默认关"O")
	private String second;
	
	//时钟显示(默认关"O")
	//第一行(4个)
	private char[] hour1 = new char[4];
	//第二行(4个)
	private char[] hour2 = new char[4];
	
	//分钟显示(默认关"O")
	//第一行(11个)
	private char[] minute1 = new char[11];
	//第二行(4个)
	private char[] minute2 = new char[4];

	//将时钟字符串转换为目标字符串显示(接口方法覆盖)
	public String convertTime(String aTime) {
		//初始化属性值
		init();
		
		//业务逻辑处理
		handleTimeString(aTime);
		
		//返回结果
		return returnTargetRes();
	}
	
	//初始化属性值
	private void init() {
		/***************初始化秒钟显示**************/
		second = "O";
		
		/***************初始化时钟显示**************/
		for(int i = 0; i < hour1.length; i++){
			hour1[i] = 'O';
		}
		for(int i = 0; i < hour2.length; i++){
			hour2[i] = 'O';
		}
		
		/***************初始化分钟显示**************/
		for(int i = 0; i < minute1.length; i++){
			minute1[i] = 'O';
		}
		for(int i = 0; i < minute2.length; i++){
			minute2[i] = 'O';
		}
		
	}

	//显示时间字符串拼接处理(回车换行)
	public String returnTargetRes(){
		return second + "\r\n" 
				+ new String(hour1) + "\r\n" + new String(hour2) + "\r\n"
				+ new String(minute1) + "\r\n" + new String(minute2);
	}
	
	//对时间字符串进行分割等业务处理
	public void handleTimeString(String timeStr){
		//将时间字符串分割成时/分/秒字符串数组
		String[] times = timeStr.split(":");
		
		/********************秒钟显示处理**********************/
		this.second = SecondEnum.valueOf(Integer.valueOf(times[2]) % 2).getFlag();
		
		/********************时钟显示处理**********************/
		//第一行时钟显示处理
		for(int i = 0; i < computeTargetNum(true,Integer.valueOf(times[0]),5); i++){
			hour1[i] = 'R';
		}
		
		//第二行时钟显示处理
		for(int i = 0; i < computeTargetNum(false,Integer.valueOf(times[0]),5); i++){
			hour2[i] = 'R';
		}
		
		/********************分钟显示处理**********************/
		//第一行分钟显示处理
		for(int i = 0; i < computeTargetNum(true,Integer.valueOf(times[1]),5); i++){
			if(i == 2 | i == 5 | i == 8) {
				//第3/6/9灯亮为红色
				minute1[i] = 'R';
			}
			else {
				//其它灯亮为黄色
				minute1[i] = 'Y';
			}
		}
		
		//第二行分钟显示处理
		for(int i = 0; i < computeTargetNum(false,Integer.valueOf(times[1]),5); i++){
			//灯亮为黄色
			minute2[i] = 'Y';
		}
		
	}
	
	//求整/求余(不考虑异常情况)
	private int computeTargetNum(boolean type, int target, int unit){
		return type ? target / unit : target % unit;
	}
	
}
