package com.paic.arch.interviews.enums;

/***
 * 
 * 关于秒显示的枚举类
 * 
 * @author Liu
 *
 */
public enum SecondEnum {
	ON(0,"Y"),OFF(1,"O");
	
	private SecondEnum(Integer val,String flag){
		this.val = val;
		this.flag = flag;
	}
	
	public static SecondEnum valueOf(Integer val){
		for(SecondEnum secondEnum : values()){
			if(secondEnum.val == val){
				return secondEnum;
			}
		}
		
		throw new IllegalArgumentException("illegal val:" + val);
	}
	
	private Integer val;
	
	private String flag;
	
	public Integer getVal() {
		return val;
	}

	public String getFlag() {
		return flag;
	}
	
}
