### Candidate Chinese Name:
* 刘浩
 
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 1、本题实际上是关于字符串处理的问题，给出一个时间字符串，输出满足要求的字符串
  2、本题采用分而治之法，先将秒/时/分逻辑各自分离出来然后有针对性地进行处理
  3、每次调用对象方法前需要重置对象所有属性值，否则可能会影响前后调用而产生错误

- - -